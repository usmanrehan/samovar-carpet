//
//  UserModel.swift
//
//  Created by Hamza Hasan on 09/09/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class UserModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let phoneNumber = "PhoneNumber"
    static let profileImageURL = "ProfileImageURL"
    static let cityID = "CityID"
    static let lastName = "LastName"
    static let iD = "ID"
    static let accountLawyerID = "AccountLawyerID"
    static let firstName = "FirstName"
    static let cityName = "CityName"
    static let isVerified = "IsVerified"
    static let role = "Role"
    static let emailID = "EmailID"
    static let countryID = "CountryID"
    static let countryName = "CountryName"
  }

  // MARK: Properties
  @objc dynamic var phoneNumber: String? = ""
  @objc dynamic var profileImageURL: String? = ""
  @objc dynamic var cityID = 0
  @objc dynamic var lastName: String? = ""
  @objc dynamic var iD = 0
  @objc dynamic var accountLawyerID = 0
  @objc dynamic var firstName: String? = ""
  @objc dynamic var cityName: String? = ""
  @objc dynamic var isVerified = false
  @objc dynamic var role = 0
  @objc dynamic var emailID: String? = ""
  @objc dynamic var countryID = 0
  @objc dynamic var countryName: String? = ""
  @objc dynamic var accessToken: String? = ""
  @objc dynamic var isGuest = false
  @objc dynamic var isSocial = false
  @objc dynamic var tokenPurchased = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "iD"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    phoneNumber <- map[SerializationKeys.phoneNumber]
    profileImageURL <- map[SerializationKeys.profileImageURL]
    cityID <- map[SerializationKeys.cityID]
    lastName <- map[SerializationKeys.lastName]
    iD <- map[SerializationKeys.iD]
    accountLawyerID <- map[SerializationKeys.accountLawyerID]
    firstName <- map[SerializationKeys.firstName]
    cityName <- map[SerializationKeys.cityName]
    isVerified <- map[SerializationKeys.isVerified]
    role <- map[SerializationKeys.role]
    emailID <- map[SerializationKeys.emailID]
    countryID <- map[SerializationKeys.countryID]
    countryName <- map[SerializationKeys.countryName]
  }


}
/*
 ["CountryID": 1, "PhoneNumber": +376123456, "CountryName": Lebanon, "IsVerified": 0, "Role": 2, "FirstName": Lawyer, "LastName": iOS, "CityID": 7, "AccountLawyerID": 11, "ProfileImageURL": http:utorny.ingicweb.com/Uploads/Images/Default/Path.ProfileImage.png, "EmailID": lawyer@mailinator.com, "ID": 51, "CityName": Tripoli]
 */
