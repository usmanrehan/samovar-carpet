//
//  ProjectEnums.swift
//  Quick Delivery
//
//  Created by macbook on 16/07/2019.
//  Copyright © 2019 Seller. All rights reserved.
//

import Foundation

enum CaseTypes:String{
    case new = "1"
    case ongoing = "2"
    case pastCases = "3"
}

enum LawyerCaseTypes:String{
    case myCases = "2"
    case myBids = "1"
    case pastCases = "3"
}

enum UploadFileType{
    case camera
    case photo
    case video
    case file
}

enum HomeUserType{
    case categories
    case subCategories
}

enum CaseFiles{
    case publicFiles
    case privateFiles
}

enum CalendarType{
    case day
    case week
    case month
}

enum BiometricType {
    case none
    case touch
    case face
}

enum SubscriptionOn {
    case signUp
    case login
}

enum NotificationType: String {
    case Admin = "1"
    case AcceptedBid = "2"
    case SuggestNewQuote = "3"
    case AcceptedNewQuote = "4"
    case ClientReceivedNewbid = "5"
    case MeetingReminder = "6"
    case NewMessage = "7"
    case ClientNewCase = "8"
    case LawyerNewCase = "9"
    case BidDecline = "10"
    case AcceptedOtherBid = "11"
    case FinalPrice = "12"
    case CaseMarkCompleted = "13"
    case LawyerReview = "14"
    case NewFile = "15"
    case NewEvent = "16"
    case VoiceNote = "17"
    case CaseReposted = "18"
}
