import Foundation
import UIKit
import LocalAuthentication

class BaseController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let notificationName = Notification.Name("PushToEditProfile")
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationObserver), name: notificationName, object: nil)
        self.hideKeyboardWhenTappedAround()
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
//        self.view.addGestureRecognizer(tap)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBar()
    }
    
}
//MARK:- Helper Methods
extension BaseController{
    func setNavigationBar(){
        let currentClassName = Utility.main.topViewController()?.className
        guard let navControllerCount = self.navigationController?.viewControllers.count else {return}
        if navControllerCount > 1{
            self.navigationController?.navigationBar.isHidden = false
            //self.setStatusBarStyle(.default)
            self.addBackBarButtonItem()
            return
        }
        else{
            self.navigationController?.navigationBar.isHidden = true
        }
        self.resetNavigationBar()
    }
    func resetNavigationBar(){
        self.navigationController?.navigationBar.barTintColor = Global.APP_COLOR_LIGHT
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }
}
//MARK:- Navigation items
extension BaseController{
    func addBackBarButtonItem() {
        let image = UIImage(named: "back")
        let backItem = UIBarButtonItem(image: image,
                                       style: .plain,
                                       target: self,
                                       action: #selector(self.onBtnBack))
        
        self.navigationItem.leftBarButtonItem = backItem
    }
    func addSkipBarButtonItem() {
        let skipItem = UIBarButtonItem(title: Strings.SKIP.text,
                                       style: .plain,
                                       target: self,
                                       action: #selector(self.onBtnSkip))
        
        self.navigationItem.rightBarButtonItem = skipItem
    }
    func addEditProfileBarButtonItem() {
        let image = UIImage(named: "editIcon")
        let backItem = UIBarButtonItem(image: image,
                                       style: .plain,
                                       target: self,
                                       action: #selector(self.onBtnEditProfile))
        
        self.navigationItem.rightBarButtonItem = backItem
    }
}
//MARK:- @objc Methods
extension BaseController{
    @objc func onBtnBack() {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func onBtnSkip() {
        self.guestLogin()
    }
    @objc func onBtnEditProfile() {
       
    }
    @objc func notificationObserver(notification: NSNotification) {
        
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        //self.view.endEditing(true)
    }
}
//MARK:- Application flow
extension BaseController{
    
}
extension UIViewController {
    var className: String {
        return NSStringFromClass(self.classForCoder).components(separatedBy: ".").last!;
    }
}
//MARK:- Services
extension BaseController{
    func guestLogin(){
        let DeviceToken = Constants.DeviceToken
        let LanguageID = LocalizationSystem.sharedInstance.getLanguage() == "en" ? 1 : 2
        
        let param:[String:Any] = ["DeviceToken":DeviceToken,"LanguageID":LanguageID]
        APIManager.sharedInstance.usersAPIManager.GuestLogin(params: param, success: { (responseObject) in
            print(responseObject)
            let guest = UserModel()
            guest.isGuest = true
            guest.firstName = Strings.GUEST.text
            guest.accessToken = Constants.accessToken
            AppStateManager.sharedInstance.guestLogin(guest: guest)
        }) { (error) in
            print(error)
        }
    }
    func registerDeviceToken(){
        let DeviceToken = Constants.DeviceToken
        let DeviceType = "2"
//        let ID = 0
        let params:[String:Any] = ["DeviceToken":DeviceToken,
                                   "DeviceType":DeviceType]
                                   //"ID":ID]
        APIManager.sharedInstance.usersAPIManager.RegisterDeviceToken(params: params, success: { (responseObject) in
            print(responseObject)
        }) { (error) in
            print(error)
        }
    }
}
