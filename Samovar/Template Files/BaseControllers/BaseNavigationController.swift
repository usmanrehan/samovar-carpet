//
//  BaseNavigationController.swift
//  The Court Lawyer
//
//  Created by Ahmed Shahid on 5/3/18.
//  Copyright © 2018 Ahmed Shahid. All rights reserved.
//

import Foundation
import UIKit

class BaseNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.barTintColor = Global.APP_COLOR_LIGHT
//        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        self.navigationBar.isTranslucent = false
    }
}
//extension UIViewController {
//    func setStatusBarStyle(_ style: UIStatusBarStyle) {
//        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
//            statusBar.backgroundColor = style == .lightContent ? Global.APP_COLOR : Global.APP_COLOR_LIGHT
//            statusBar.setValue(style == .lightContent ? UIColor.white : .black, forKey: "foregroundColor")
//        }
//    }
//}
