import Foundation
import UIKit
import RealmSwift

class AppStateManager: NSObject {
    
    static let sharedInstance = AppStateManager()
    var loggedInUser: UserModel!
    var realm: Realm!
    
    override init() {
        super.init()
        if(!(realm != nil)){
            realm = try! Realm()
        }
        loggedInUser = realm.objects(UserModel.self).first
    }
    
    func isUserLoggedIn() -> Bool{
        if (self.loggedInUser) != nil {
            if self.loggedInUser.isInvalidated {
                return false
            }
            return true
        }
        return false
    }
    func saveUser(user:UserModel) {
        try! Global.APP_REALM?.write(){
            AppStateManager.sharedInstance.loggedInUser = user
            Global.APP_REALM?.add(user, update: .all)
        }
    }
    func loginUser(user:UserModel) {
        try! Global.APP_REALM?.write(){
            AppStateManager.sharedInstance.loggedInUser = user
            Global.APP_REALM?.add(user, update: .all)
        }
        AppDelegate.shared.changeRootViewController()
        
        let loggedInUserID = Constants.USER_DEFAULTS.value(forKey: "userID") as? Int
        if loggedInUserID == nil{
            let userID = user.iD
            Constants.USER_DEFAULTS.set(userID, forKey: "userID")
            
            let isUser = AppStateManager.sharedInstance.loggedInUser.role == 1 ? true:false
            if isUser{
                Constants.USER_DEFAULTS.set(true, forKey: "userType")
            }
            else{
                Constants.USER_DEFAULTS.set(false, forKey: "userType")
            }
        }
    }
    func logoutUser(){
        DispatchQueue.main.async {
            Utility.main.showAlert(message: Strings.ASK_LOGOUT.text, title: Strings.LOGOUT.text, controller: Utility.main.topViewController()!) { (yes, no) in
                if yes != nil{
                    self.processLogoutUser()
                }
            }
        }
    }
    func logoutUserOnly(){
        try! Global.APP_REALM?.write() {
            Global.APP_REALM?.deleteAll()
            self.loggedInUser = nil
        }
    }
    func logoutUserAndChangeRootView(){
        try! Global.APP_REALM?.write() {
            Global.APP_REALM?.deleteAll()
            self.loggedInUser = nil
        }
        AppDelegate.shared.changeRootViewController()
    }
}
extension AppStateManager{
    func guestLogin(guest:UserModel){
        try! Global.APP_REALM?.write(){
            AppStateManager.sharedInstance.loggedInUser = guest
            Global.APP_REALM?.add(guest, update: .all)
        }
        AppDelegate.shared.changeRootViewController()
    }
    func isGuestLoggedIn()->Bool{
        if self.loggedInUser != nil {
            if self.loggedInUser.isGuest {
                return true
            }
        }
        return false
    }
}
extension AppStateManager{
    private func processLogoutUser(){
        let DeviceToken = Constants.DeviceToken
        let param:[String:Any] = ["DeviceToken":DeviceToken]
        APIManager.sharedInstance.usersAPIManager.LogoutUser(params: param, success: { (responseObject) in
            print(responseObject)
            try! Global.APP_REALM?.write() {
                Global.APP_REALM?.delete(self.loggedInUser)
                self.loggedInUser = nil
            }
            AppDelegate.shared.changeRootViewController()
        }) { (error) in
            print(error)
        }
    }
}
