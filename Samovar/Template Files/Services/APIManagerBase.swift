import Foundation
import UIKit
import Alamofire
import SwiftyJSON

enum Route: String {
    //MARK:- USER
    case UserSignUp = "Account/UserSignUp"
    case UserEditProfile = "Account/UserEditProfile"
    case Login = "Account/SignIn"
    case socialLogin = "Account/SocialSignIn"
    case Logout = "Account/Logout"
    case resendCode = "Account/ResendCode"
    case registerPhoneNumber = "Account/PhoneNumberRegister"
    case phoneVerification = "Account/PhoneVerification"
    case Guest = "Account/GuestSignIn"
    case ChangePassword = "Account/ChangePassword"
    case Fingerprint = "Account/Fingerprint"
    case RegisterDeviceToken = "Account/DeviceRegister"
    case changeLanguage = "Account/ChangeLanguage"
    case buySubscription = "Account/AccountPlanPackage"
    
    //MARK:- GENERAL
    case getAboutUs = "General/AboutUs"
    case getContent = "General/Content"
    case getCountries = "General/Countries"
    case getCities = "General/Cities"
    case getLanguages = "General/Languagies"
    case getSubscriptions = "General/PlanPackages"
    case getActiveAccountActivePlan = "Account/Tokens"
    case getCategories = "General/Categories"
    case getCaseTypes = "General/CaseTypes"
    case getFeaturesRates = "Case/CaseUrgentFeatureRates"
    case getNotifications = "General/Notifications"
    case getNotificationsCount = "General/NotificationCount"
    case deleteNotifications = "General/DeleteNotification"
    case clearAllNotifications = "General/ClearAllNotification"
    case clearNotificationCount = "General/NotificationView"

    //MARK:- LAWYER
    case SignUpLawyer = "Account/LawyerSignUp"
    case lawyerProfile = "General/LawyerProfile"
    case lawyerCases = "Case/LawyerCases"
    case attachDocument = "Case/ActivityAttachedFiles"
    case addNote = "Case/ActivityNote"
    case addReminder = "Case/ActivityCalendar"
    case audio = "Case/ActivityRecordAudio"
    case accountDetails = "Account/AccountDetail"
    case lawyerEditProfile = "Account/LawyerEditProfile"
    
    //MARK:- CASE
    case conversationList = "Case/ConversationList"
    case newCase = "Case/PostNewCase"
    case userCasesByCategory = "Case/SearchHomeCases"
    case userCases = "Case/UserCases"
    case totalCases = "Case/TotalCases"
    case caseActivities = "Case/CaseActivities"
    case caseScheduleCalendars = "Case/ScheduleCalendars"
    case caseDetail = "Case/CaseDetail"
    case conversations = "Case/Conversations"
    case sendMessage = "Case/Conversation"
    case bidDetail = "Case/BidDetail"
    case acceptBid = "Case/BidAccept"
    case rejectBid = "Case/BidDecline"
    case suggestNewQuote = "Case/SuggestNewQuote"
    case markComplete = "Case/MarkCompleted"
    case bidCase = "case/BidSubmit"
    case finalPrice = "Case/FinalPrice"
    case caseBidDetail = "Case/Bid"
    
}
class APIManagerBase: NSObject {
    let baseURL = Constants.BaseURL
    let defaultRequestHeader = ["Content-Type": "application/json"]
    let defaultError = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Request Failed."])
    
    func getAuthorizationHeader () -> Dictionary<String,String> {
        if let token = APIManager.sharedInstance.serverToken {
            return ["Authorization":"\(token)"]
        }
        return ["Content-Type":"application/x-www-form-urlencoded"]
    }
    
    func URLforRoute(route: String,params:[String: Any]) -> NSURL? {
        if let components: NSURLComponents  = NSURLComponents(string: (Constants.BaseURL+route)){
            var queryItems = [NSURLQueryItem]()
            for(key,value) in params {
                queryItems.append(NSURLQueryItem(name:key,value: "\(value)"))
            }
            components.queryItems = queryItems as [URLQueryItem]?
            return components.url as NSURL?
        }
        return nil
    }
    
    func POSTURLforRoute(route:String) -> URL?{
        
        if let components: NSURLComponents = NSURLComponents(string: (Constants.BaseURL+route)){
            return components.url! as URL
        }
        return nil
    }
    
}
//MARK:- Response Handle
extension APIManagerBase{
    fileprivate func responseResult(_ response:DataResponse<Any>,
                                    success: @escaping (_ response: AnyObject) -> Void,
                                    failure: @escaping (_ error: NSError) -> Void) {
        Utility.hideLoader()
        let errorGenericMessage = Strings.ERROR_GENERIC_MESSAGE.text + "\(response.response?.url?.absoluteString ?? "")"
        switch response.result{
        case .success:
            if let dictData = response.result.value as? NSDictionary {
                if let token = response.response?.allHeaderFields["Token"] as? String{
                    Constants.accessToken = token
                }
                if let status = dictData["Response"] as? Bool{
                    if status{
                        success(dictData["Result"] as AnyObject)
                        return
                    }
                    else{
                        if let error = (dictData["Message"] as? String){
                            Utility.main.showAlert(message: error, title: Strings.ERROR.text)
                        }
                        else{
                            Utility.main.showAlert(message: errorGenericMessage, title: Strings.ERROR.text)
                        }
                    }
                }
            } else {
                //Failure
                let errorMessage: String = Strings.UNKNOWN_ERROR.text + "\(response.response?.url?.absoluteString ?? "")"
                let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                let error = NSError(domain: "Domain", code: 0, userInfo: userInfo);
                failure(error)
//                Utility.main.showAlert(message: errorGenericMessage, title: Strings.ERROR.text)
            }
        case .failure(let error):
            failure(error as NSError)
            Utility.main.showAlert(message: errorGenericMessage, title: Strings.ERROR.text)
        }
    }
}

//MARK:- Get APIs
extension APIManagerBase{
    
    func getArrayResponseWith(route: URL,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure,
                        withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {
                    response in
                    if let arrayResponse = response as? Array<AnyObject>{
                        success(arrayResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })}
        }
        else{
            Alamofire.request(route, method: .get, parameters: nil, encoding: URLEncoding()).responseJSON {
                response in
                
                self.responseResult(response, success: {response in
                    if let arrayResponse = response as? Array<AnyObject>{
                        success(arrayResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func getDictionaryResponseWith(route: URL,
                                     success:@escaping DefaultAPISuccessClosure,
                                     failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .get, parameters: nil, encoding: URLEncoding()).responseJSON {
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func getBoolResponseWith(route: URL,
                               success:@escaping DefaultBoolResultAPISuccesClosure,
                               failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    success(true)
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .get , parameters: nil, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    success(true)
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }

    func getStringResponseWith(route: URL,
                                 success:@escaping DefaultStringResultAPISuccesClosure,
                                 failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let stringResponse = response as? String{
                        success(stringResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .get , parameters: nil, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let stringResponse = response as? String{
                        success(stringResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }

    func getIntResponseWith(route: URL,
                             success:@escaping DefaultIntResultAPISuccesClosure,
                             failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let intResponse = response as? Int{
                        success(intResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .get , parameters: nil, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let intResponse = response as? Int{
                        success(intResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
}
//MARK:- Post APIs
extension APIManagerBase{
    
    func postArrayResponseWith(route: URL,parameters: Parameters,
                              success:@escaping DefaultArrayResultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure, withHeaders:Bool){
        
        if withHeaders {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let arrayResponse = response as? [AnyObject]{
                        success(arrayResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let arrayResponse = response as? [AnyObject]{
                        success(arrayResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func postDictionaryResponseWith(route: URL,parameters: Parameters,
                         success:@escaping DefaultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure, withHeader:Bool){
        
        if withHeader{
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                        //Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func postDictionaryResponseWithURLEncoding(route: URL,parameters: Parameters,
                                    success:@escaping DefaultAPISuccessClosure,
                                    failure:@escaping DefaultAPIFailureClosure, withHeader:Bool){
        
        if withHeader{
            Alamofire.request(route, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func postMultipartDictionaryResponseWith(route: URL,parameters: Parameters,
                                  success:@escaping DefaultAPISuccessClosure,
                                  failure:@escaping DefaultAPIFailureClosure , withHeader: Bool){
        if withHeader{
            Alamofire.upload(multipartFormData:{ multipartFormData in
                for (key , value) in parameters {
                    if let data:Data = value as? Data {
                        let file_key = key.components(separatedBy: CharacterSet.decimalDigits).joined()
                        multipartFormData.append(data, withName: file_key, fileName: "\(data.getExtension)", mimeType: "\(data.mimeType)")
                    } else {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                    }
                    print(multipartFormData)
                }
            },
                             usingThreshold:UInt64.init(),
                             to: route,
                             method:.post,
                             headers: getAuthorizationHeader(),
                             encodingCompletion: { result in
                                switch result {
                                case .success(let upload, _, _):
                                    upload.responseJSON { response in
                                        self.responseResult(response, success: {result in
                                            if let dictionaryResponse = result as? Dictionary<String, AnyObject>{
                                                success(dictionaryResponse)
                                            }
                                            else{
                                                success(Dictionary<String, AnyObject>())
                                                //Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                                            }
                                        }, failure: {error in
                                            
                                            failure(error)
                                        })
                                    }
                                case .failure(let encodingError):
                                    failure(encodingError as NSError)
                                }
            }
                
            )
            
            
        }else{
            Alamofire.upload (
                multipartFormData: { multipartFormData in
                    for (key , value) in parameters {
                        if let data:Data = value as? Data {
                            multipartFormData.append(data, withName: key, fileName: "\(data.getExtension)", mimeType: "\(data.mimeType)")
                        } else {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                        }
                    }
                    
            },
                to: route,
                encodingCompletion: { result in
                    switch result {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            self.responseResult(response, success: {result in
                                if let dictionaryResponse = result as? Dictionary<String, AnyObject>{
                                    success(dictionaryResponse)
                                }
                                else{
                                    Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                                }
                            }, failure: {error in
                                
                                failure(error)
                            })
                        }
                    case .failure(let encodingError):
                        failure(encodingError as NSError)
                    }
            }
            )
        }
    }

    func postBoolResponseWith(route: URL,parameters: Parameters,
                                success:@escaping DefaultBoolResultAPISuccesClosure,
                                failure:@escaping DefaultAPIFailureClosure, withHeader:Bool){
        
        if withHeader{
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    success(true)
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    success(true)
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }

    func postStringResponseWith(route: URL,
                               success:@escaping DefaultStringResultAPISuccesClosure,
                               failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .post, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let stringResponse = response as? String{
                        success(stringResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .post , parameters: nil, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let stringResponse = response as? String{
                        success(stringResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func postIntResponseWith(route: URL,
                               success:@escaping DefaultIntResultAPISuccesClosure,
                               failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if withHeader{
            Alamofire.request(route, method: .post, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let intResponse = response as? Int{
                        success(intResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        else{
            Alamofire.request(route, method: .post , parameters: nil, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let intResponse = response as? Int{
                        success(intResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
}
extension APIManagerBase{
    func deleteDictionaryResponseWith(route: URL,parameters: Parameters,
                                    success:@escaping DefaultAPISuccessClosure,
                                    failure:@escaping DefaultAPIFailureClosure, withHeader:Bool){
        
        if withHeader{
            Alamofire.request(route, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .delete, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
}
//MARK:- Put APIs
extension APIManagerBase{
    
    func putDictionaryResponseWith(route: URL,parameters: Parameters,
                                    success:@escaping DefaultAPISuccessClosure,
                                    failure:@escaping DefaultAPIFailureClosure, withHeader:Bool){
        
        if withHeader{
            Alamofire.request(route, method: .put, parameters: parameters, encoding: URLEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .put, parameters: parameters, encoding: URLEncoding.default).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    if let dictionaryResponse = response as? Dictionary<String, AnyObject>{
                        success(dictionaryResponse)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func putMultipartDictionaryResponseWith(route: URL,parameters: Parameters,
                                             success:@escaping DefaultAPISuccessClosure,
                                             failure:@escaping DefaultAPIFailureClosure , withHeader: Bool){
        if withHeader{
            Alamofire.upload(multipartFormData:{ multipartFormData in
                for (key , value) in parameters {
                    if let data:Data = value as? Data {
                        multipartFormData.append(data, withName: key, fileName: "\(data.getExtension)", mimeType: "\(data.mimeType)")
                    } else {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                    }
                }
            },
                             usingThreshold:UInt64.init(),
                             to: route,
                             method:.put,
                             headers: getAuthorizationHeader(),
                             encodingCompletion: { result in
                                switch result {
                                case .success(let upload, _, _):
                                    upload.responseJSON { response in
                                        self.responseResult(response, success: {result in
                                            if let dictionaryResponse = result as? Dictionary<String, AnyObject>{
                                                success(dictionaryResponse)
                                            }
                                            else{
                                                Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                                            }
                                        }, failure: {error in
                                            
                                            failure(error)
                                        })
                                    }
                                case .failure(let encodingError):
                                    failure(encodingError as NSError)
                                }
            }
                
            )
            
            
        }else{
            Alamofire.upload (
                multipartFormData: { multipartFormData in
                    for (key , value) in parameters {
                        if let data:Data = value as? Data {
                            multipartFormData.append(data, withName: key, fileName: "\(data.getExtension)", mimeType: "\(data.mimeType)")
                        } else {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                        }
                    }
                    
            },
                to: route,
                encodingCompletion: { result in
                    switch result {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            self.responseResult(response, success: {result in
                                if let dictionaryResponse = result as? Dictionary<String, AnyObject>{
                                    success(dictionaryResponse)
                                }
                                else{
                                    Utility.main.showAlert(message: Strings.RESPONSE_ERROR.rawValue + "\(route)", title: Strings.ERROR.text)
                                }
                            }, failure: {error in
                                
                                failure(error)
                            })
                        }
                    case .failure(let encodingError):
                        failure(encodingError as NSError)
                    }
            }
            )
        }
    }
}

public extension Data {
    var mimeType:String {
    get {
        var c = [UInt32](repeating: 0, count: 1)
        (self as NSData).getBytes(&c, length: 1)
        switch (c[0]) {
        case 0xFF:
            return "image/jpeg";
        case 0x89:
            return "image/png";
        case 0x47:
            return "image/gif";
        case 0x49, 0x4D:
            return "image/tiff";
        case 0x25:
            return "application/pdf";
        case 0xD0:
            return "application/vnd";
        case 0x46:
            return "text/plain";
        default:
            print("mimeType for \(c[0]) in available");
            return "application/octet-stream";
        }
    }
}
    var getExtension: String {
    get {
        var c = [UInt32](repeating: 0, count: 1)
        (self as NSData).getBytes(&c, length: 1)
        switch (c[0]) {
        case 0xFF:
            return "_IMG.jpeg";
        case 0x89:
            return "_IMG.png";
        case 0x47:
            return "_IMG.gif";
        case 0x49, 0x4D:
            return "_IMG.tiff";
        case 0x25:
            return "_FILE.pdf";
        case 0xD0:
            return "_FILE.vnd";
        case 0x46:
            return "_FILE.txt";
        default:
            print("mimeType for \(c[0]) in available");
            return "_video.mp4";
        }
    }
}
}
