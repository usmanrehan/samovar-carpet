
import Foundation
import UIKit

struct Global{
    static let LOGGED_IN_USER                = AppStateManager.sharedInstance.loggedInUser
    static var APP_MANAGER                   = AppStateManager.sharedInstance
    static var APP_REALM                     = APP_MANAGER.realm
    static var APP_COLOR                     = UIColor(red:97/255, green:37/255, blue:42/255, alpha:1.0)
    static var APP_COLOR_LIGHT               = UIColor(red:245/255, green:248/255, blue:249/255, alpha:1.0)
}

struct Constants {
    //MARK:- Base URL
    static let BaseURL                     = "http://utornyadmin.ingicweb.com/api/"
    static let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
    static let UIWINDOW                    = UIApplication.shared.delegate!.window!
    static let USER_DEFAULTS               = UserDefaults.standard
    static let SINGLETON                   = Singleton.sharedInstance
    static let DEFAULTS_USER_KEY           = "User"
    static var DeviceToken                 = "No certificates"
    static let serverDateFormat            = "yyyy-MM-dd'T'HH:mm:ss.SSS"//"yyyy-MM-dd HH:mm:ss""
    static let PAGINATION_PAGE_SIZE        = 100
    static var adminPhone                  = "123456"
    //MARK:- Notification observer names
    static let NotificationCount           = "NotificationCount"
    static let apiKey                      = "AIzaSyCDylplefNyWlLDoBL_n2VFjwlMWvq3sBg"
    static var accessToken                 = ""
    static var GoogleSignInClientID        = "821622832147-00elb5glohhc0njcl57bksf4b9kftomc.apps.googleusercontent.com"
    static var KeyboardHeight:CGFloat      = 0.0
    
}
