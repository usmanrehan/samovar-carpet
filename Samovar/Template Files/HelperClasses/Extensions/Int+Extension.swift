//
//  Int+Extension.swift
//  Utorny
//
//  Created by Mac Book on 14/01/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation

extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}
